# Wacky-db - documentation
this database was made by me in a few hours. expect it to be (somewhat) buggy. if you need any help DM me on discord. my name there is **Family Friendly#0001**

# offline database documentation

## constructors
| path         | the path to save the data on                    |
|--------------|-------------------------------------------------|
| --options--  | (obj) - (not forced)                            |
| strict       | true/false (default false)                      |
| settingsPath | the path to save db settings on. (locked files) |
## write(header,data)
Will add a new record with the passed data. If the strict option is turned of running this command on a already existing record will update it. if the record is locked or the database is set to strict you will get an error.

## update(header,data)
will update the record with the passed data. if the record is locked you will recieve an error.

## get(?header)
will return all records from db. if header is passed it will only pass the data of that record

## exists(header)
will return true if record exists. false if not

## ensure(header, data)
ensures record exist with the passed data. if it does not exist the record wil be created.

## math(header,prop,math)
will execute the passed math(string) on the property and save.

## updateprop(header,prop,data)
will update a specific property of a record.

## deleteprop(header,prop)
will delete a specific property of a record.

## delete(header)
will delete this record

## delete("\*\*DELETEALL\*\*")
will delete all records

## lockrecord(header)
will lock the record. this will ensure it isnt written over/changed by mistake. to unlock it simply run the function again.

## param explained
| header | the ID of the record                           |
|--------|------------------------------------------------|
| prop   | the property of a record (obj.prop)            |
| data   | the data to save (json)                        |
| math   | the math to execute on prop (prop * 345 / 435) |

# wacky-db server configuration
## constructors

```js
new db.server({location:"./filepath.json", port:3000, SecretToken:3})
```
## token perm level explained
* 3 **write, delete** + anything below
* 2 **updateProp,  math** + anything below
* 1 **exists, get**

# wacky-db online client documentation
## connect
```js
new db.dbOnlineClient("https://SERVER-LOCATION.com", "SecretToken")
```

## write(**header, data**)
writes data to remote database
## delete(**header**)
deletes record from online database
## get(**?header**)
gets data from remote database. if header is empty it gets all

# code snippets

## simple discord-bot coin command
please keep in mind this wont likely work in your case. this is just to show how wacky-db works.
```js
var _db = require("wacky-db");
var db = new _db.database("./coolPath.json");

client.on("Message", (message) => {
if(!db.exists(message.author.id)) db.write(message.author.id, {coins:1});
let args = message.content.split(" ");
switch(args[0])
{
case "collect":
let random = Math.floor(Math.random() * 6);
db.math(message.author.id,"coins", `+${random}`);
message.channel.send(`collected ${random} coins!`)
break;
case "money":
let data = db.get(message.author.id);
message.channel.send(`you have ${data.coins} ${data.coins > 1 ? "coins" : "coin"}`);
break;
}
})
```

## online server. create & connect
this server will be created on localhost 3000. this is not optional.
```js
var _db = require("wacky-db");
new _db.server({location:"./path.json", SecretToken:3});
var db = new _db.dbOnlineClient("https://localhost:3000", "SecretToken");
db.write("hello","world");
```

# server endpoints (POST)
there are more endpoints then these. I just cant bother listing them all..

## host/!!TOKEN!!/write
### body 
* header: the name of this record
* data: the data to save to this record

## host/!!TOKEN!!/delete
### body 
* header: the name of this record.






