var fs = require("fs");



/**
 * @Documentation 
 * Creates a new wacky-db json database. Read the more detailed documentations [HERE](https://gitlab.com/familyfriendly/wackydb/blob/master/README.md).... 
 *  @example
 * var db = new database("./json.json")
 * @returns instance of a wacky-db JSON database 
 *@discord ![alt](https://ui-ex.com/images/discord-transparent-text-1.png) 
 * @discordtxt [**JOIN MY DISCORD SERVER**](https://discord.gg/Vn8twW7)
 */
exports.database = class database {
/**
 * 
 * @param {String} path 
 * @param {Object=} options 
 * @param {String=} options.header epic
 */
    constructor(path, options) {
        //ensure path is passed
        if (!path) {
            throw new Error("no path defined in class creation.");
        }
        this.path = path;

        //strict settings
        if (options && options.strict && options.strict === true) {
            this.strict = true;
        } else {
            this.strict = false;
        };

        //make sure the file exists
        if (!fs.existsSync(path)) {
            fs.writeFile(this.path, '{}', function(err) {
                if (err) console.error(err);
            });
        }

        if (options && options.settingsPath) this.settingsPath = options.settingsPath;
        else this.settingsPath = "./DBsettings.json";

    }

    //psuedo function retrieve data
    _getdata() {
        var data = JSON.parse(fs.readFileSync(this.path, 'utf8'));
        return data;
    }


    //write data
    /**
     * @documentation Writes the data with the selected header as the ID.
     * @exapmle {"header":{"data":"hi"}}
     * @param {String} header 
     */
    write(header, _data) {
        var data = this._getdata();

        if (!header || !_data) throw new Error("no data/header")


        if (!data[header]) data[header] = _data;
        else if (this.strict === true) {
            throw new Error("(strict) can't write to existing file. use the update function instead");

        } else {
            if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
            data[header] = _data;
        }

        fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
            throw err
        })
    }

    /**
     * @documentation updates the selected header with the provided data
     * @param {String} header 
     */
    update(header, _data) {
        if (!header || !_data) throw new Error(`you did not pass any header or data`);
        var data = this._getdata();
        if (!data[header]) throw new Error(`record "${header}" does not exist in ${this.path}`);
        if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
        data[header] = _data;
        fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
            throw err
        })
    }

    /**
     * @documentation if no header is provided ALL data will be returned
     * @returns data
     * @param {String=} header 
     */
    get(header) {
        var data = this._getdata();
        if (header && data[header]) return data[header];
        else return data;
    }

    
    /**
     * @documentation executes the math string on the selected data
     * @todo add nested obj property selecting. eg: object.nested.nested instead of only obj.prop
     * @param {String} header 
     * @param {String} prop 
     * @param {String} _math 
     */
    math(header, prop, _math) {
        const math = require("mathjs");
        var data = this._getdata();
        if (!header || !prop || !data[header] || !math) throw new Error(`no header/property/math passed${header ? ` or the record ${header} does not exist on ${this.path}`:``}`);
        if (typeof data[header][prop] == "undefined") throw new Error(`property ${prop} does not exist on ${header}`);
        if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
        _math = data[header][prop] + _math;
        data[header][prop] = math.eval(_math);
        fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
            throw err
        });
    }

    /**
     * @documentation checks if the record exists
     * @returns true/false
     * @param {String} header 
     */
    exists(header)
    {
        var data = this._getdata();
        if(!header) throw new Error("no header passed");
        if(typeof data[header] == "undefined") return false;
        else return true;
    }

    /**
     * @documentation checks if a record with provided header exists with the object properties. if it does not exists it creates it with the provided obj
     * @param {String} header 
     * @param {Object} _data 
     */
    ensure(header, _data)
    {
        const _ld = require("lodash");
        var data = this._getdata();
        if(!header || !_data) throw new Error(`no ${header? "":"header"}${!header && !_data? " and ":""}${_data? "":"data"} passed`);
        if(data[header] && _ld.isEqual(data[header], _data)) return;
        else{ data[header] = _data;
        fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
            throw err
        });}
    }

        /**
     * @documentation updates the property of a record obj
     * @todo add nested obj property selecting. eg: object.nested.nested instead of only obj.prop
     * @param {String} header 
     * @param {String} prop 
     * @param {String} _data 
     */
    updateprop(header, prop, _data) {
        var data = this._getdata();
        if (!header || !data[header] || !prop || !_data) throw new Error(`no header/property/data passed${header ? ` or the record ${header} does not exist on ${this.path}`:``}`);
        if (typeof data[header][prop] == "undefined") throw new Error(`property ${prop} does not exist on ${header}`);
        if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
        data[header][prop] = _data;
        fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
            throw err
        });
    }

         /**
     * @documentation deletes the property of a record obj
     * @todo add nested obj property selecting. eg: object.nested.nested instead of only obj.prop
     * @param {String} header 
     * @param {String} prop 
     */
    deleteprop(header, prop) {
        var data = this._getdata();
            if (!header || !prop || !data[header] || !data) throw new Error(`no header/property/data passed${header ? ` or the record ${header} does not exist on ${this.path}`:``}`);
            if (typeof data[header][prop] == "undefined") throw new Error(`property ${prop} does not exist on ${header}`);
            if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
            delete data[header][prop];
            fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
                throw err
            });
    }

    /**
     * @documentation deletes the record. if header is "\*\*DELETEALL\*\*" all headers will  be deleted
     * @param {String} header 
     */
    delete(header) {
        var data = this._getdata();

        if(header && header == "**DELETEALL**") {

            data = {};

            fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
                throw err
            });
            

        } else 
        {
            if (!header || !data[header]) throw new Error(`${header} cant be found on ${this.path}`);
            if (this._lockrecord(header)) throw new Error(`record ${header} is locked`);
            delete data[header];
            fs.writeFileSync(this.path, JSON.stringify(data), (err) => {
                throw err
            });

        }


    }

    /**
     * 
     * @deprecated dont use this unless you must.
     */
    lockrecord(header) {

        if (!fs.existsSync(this.settingsPath)) {
            fs.writeFile(this.settingsPath, '{}', function(err) {
                if (err) console.error(err);
            });
        }
        var data = JSON.parse(fs.readFileSync(this.settingsPath, 'utf8'));
        if (!data[header]) data[header] = "locked";
        else delete data[header];
        fs.writeFileSync(this.settingsPath, JSON.stringify(data), (err) => {
            throw err
        })
    }

    _lockrecord(header) {
        if (!fs.existsSync(this.settingsPath)) {
            return undefined;
        }
        var data = JSON.parse(fs.readFileSync(this.settingsPath, 'utf8'));
        if (data[header]) return true;
    }



}



function errorMessage(thing)
{

  var explained = ["-","no header passed.", "not authorised", "insufficient data"];


    return {ERROR: thing.message, ERROR_CODE_NUMBER: `${thing.code} - ${explained[thing.code]}` } 
}

function successmessage(text)
{
    return {SUCCESS: text}
}

exports.server = class server {
constructor(options)
{
    if(!options) throw new Error("no options passed");
    if(!options.port) {this.port = 3000; console.warn(`no port was passed. will defualt to 3000...`)}
    else this.port = options.port;
    if(!options.location)  throw new Error("no location passed");
    this.options = options;
    var db = new exports.database(options.location);
    var express = require('express');
    var app = express();

    function checkPerms(req, options, type)
    {
        let types = [];
        types["write"] = 3;
        types["get"] = 1;
        types["delete"] = 3;
        types["exists"] = 1;
        types["math"] = 2;
        types["updateprop"] = 2;

        if(!req.params.token || !options[req.params.token]) return false;
        else if(options[req.params.token] < types[type]) return false;
        else return true;
    
    }
    var bodyParser = require("body-parser");
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());






app.post(`/:token/write`, function(req, res) {
    if(!req.body || !req.body.data || !req.body.header) return res.send(errorMessage({message:`beep boop... ${req.body ? req.body : "no body"}`, code:3}))
    if(checkPerms(req, options, "write")) {
        try
        {db.write(req.body.header, req.body.data)
        }catch(err) {res.send(err)}
        res.send(successmessage("DONE!"));
    } else return res.send(errorMessage({message:`beep boop...`, code:2}))
})

app.post(`/:token/delete`, function(req, res) {
    if(!req.body || !req.body.header) return res.send(errorMessage({message:`beep boop...`, code:3}))
    if(checkPerms(req, options, "delete")) {
        try {
        db.delete(req.body.header)
    }catch(err) {return res.send(err)}
        res.send(successmessage("DELETE DONE"));
    } else return res.send(errorMessage({message:`beep boop...`, code:2}))
})

app.post(`/:token/get`, function(req, res) {
    console.log(req.body);
    if(checkPerms(req, options, "get")) {
        if(req.body && req.body.header)
        {res.send(db.get(req.body.header));}
        else res.send(db.get());    
    } else return res.send(errorMessage({message:`beep boop...`, code:2}))
})

app.post(`/:token/exists`, function(req, res) {
    if(!req.body || !req.body.header) return res.json(errorMessage({message:`beep boop...`, code:3}))
    if(checkPerms(req, options, "exists")) {
       res.send(db.exists(req.body.header));
    } else return res.json(errorMessage({message:`beep boop...`, code:2}))
})

app.post(`/:token/math`, function(req, res) {
    if(!req.body || !req.body.header || !req.body.prop ||!req.body.math) return res.send(errorMessage({message:`beep boop...`, code:3}))
    if(checkPerms(req, options, "math")) {
        try {
        db.math(req.body.header, req.body.prop, req.body.math)
    }catch(err) {return res.send(err)}
       res.send(successmessage("mathz done!"));
    } else return res.send(errorMessage({message:`beep boop...`, code:2}))
})

app.post(`/:token/updateprop`, function(req, res) {
    if(!req.body || !req.body.header || !req.body.prop ||!req.body.data) return res.send(errorMessage({message:`beep boop...`, code:3}))
    if(checkPerms(req, options, "updateprop")) {
        try {
        db.updateprop(req.body.header, req.body.prop, req.body.data)
        }catch(err) {return res.send(err)}
       res.send(successmessage("UPDATED PROP"));
    } else return res.send(errorMessage({message:`beep boop...`, code:2}))
})

    app.listen(this.port);
    console.log(`running on port ${this.port}... `);
    console.log(`---------------------------- \n thank you for using wacky-db!`)
}
/*
this is the part where I require stuff and things. also a bit of a notepad. if you are reading this then hello! hope this software will suit your purpose.
 I intentionally used the word "software" to look cool. even though you arent seeing me so that dosent make much sence at all. hahaha just ignore my rambling.
*/
  
}




/**
 * @example 
 * const client = new dbOnlineClient("localhost:3000","SECRET")
 */
exports.dbOnlineClient = class dbOnlineClient
{
    /** 
     * @param {URL} url 
     * @param {String} header
    */
    constructor(url, header)
    {
        if(!url || !header) throw new Error("no url or header passed");
        this.opt = {url: url, header: header};
        var fetch = require('node-fetch');
        this.callApi = async (url,body) =>
        {
            let temp;
            await fetch(url, {
                method:"post",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                body: JSON.stringify(body)
            })
            .then(res => res.json())
            .then(body => temp = Promise.resolve(body));
            return temp;
        }
    }

    write(header, data)
    {
        if(!header ||!data ) return console.warn("no data/header");
        let zdata = this.callApi(`${this.opt.url}/${this.opt.header}/write`, {header:header, data:data})
        return zdata;
        
    }

    delete(header)
    { 
        if(!header) return console.warn("no header");
       let data =  this.callApi(`${this.opt.url}/${this.opt.header}/delete`, {header:header})
        return data;
    }

    get(header)
    {
        var data = this.callApi(`${this.opt.url}/${this.opt.header}/get`, {header:header});  
        return data;
    }

    exists(header)
    {
        var data = this.callApi(`${this.opt.url}/${this.opt.header}/exists`, {header:header});  
        return data;
    }

    math(header, prop, math)
    {

        if(!header || !prop || !math) return console.warn("did not pass header/prop/math");
    if(!header) return console.warn("no header passed");
    let data = this.callApi(`${this.opt.url}/${this.opt.header}/math`, {header:header, prop:prop, math:math});
     return data;
    }

    updateprop(header, prop, math)
    {
        if(!header || !prop || !math) return console.warn("did not pass header/prop/data");
    if(!header) return console.warn("no header passed");
   let data = this.callApi(`${this.opt.url}/${this.opt.header}/updateprop`, {header:header, prop:prop, data:math});
        return data;
    }
}



